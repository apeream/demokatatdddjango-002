# -*- coding: utf-8 -*-

from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class FunctionalTest(TestCase):

    def setUp(self):
        self.browser= webdriver.Chrome()

    #def tearDown(self):
        #self.browser.quit()

    def atest_title(self):

        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def atest_registro(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(10)
        link = self.browser.find_element_by_id('id_register')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Juan Daniel')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Arevalo')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('5')

        self.browser.find_element_by_xpath("//select[@id='id_tiposDeServicio']/option[text()='Desarrollador Web']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3173024578')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('jd.patino1@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_imagen')
        imagen.send_keys('C:\Negra.jpg')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('juan645')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)
        span=self.browser.find_element_by_xpath('//span[text()="Juan Daniel Arevalo"]')

        self.assertIn('Juan Daniel Arevalo', span.text)

    def atest_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span=self.browser.find_element_by_xpath('//span[text()="Juan Daniel Arevalo"]')
        span.click()
        self.browser.implicitly_wait(3)
        h2=self.browser.find_element(By.XPATH, '//h2[text()="Juan Daniel Arevalo"]')

        self.assertIn('Juan Daniel Arevalo', h2.text)

    def atest_login(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(3)
        link = self.browser.find_element_by_id('id_login')
        link.click()
        self.browser.implicitly_wait(3)

        nombreUsuario = self.browser.find_element_by_id('username')
        nombreUsuario.send_keys('juan645')

        clave = self.browser.find_element_by_id('password')
        clave.send_keys('clave123')

        btn = self.browser.find_element_by_id('btn_login')
        btn.click()
        self.browser.implicitly_wait(3)

        lnk_editar = self.browser.find_element_by_id('id_editar')
        self.assertIn('Editar', lnk_editar.text)

    def atest_edicion(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(3)
        link = self.browser.find_element_by_id('id_login')
        link.click()
        self.browser.implicitly_wait(3)

        nombreUsuario = self.browser.find_element_by_id('username')
        nombreUsuario.send_keys('juan645')

        clave = self.browser.find_element_by_id('password')
        clave.send_keys('clave123')

        btn = self.browser.find_element_by_id('btn_login')
        btn.click()
        self.browser.implicitly_wait(3)
        cerrarAlert=self.browser.find_element_by_xpath('//a[text()="×"]')
        cerrarAlert.click()

        lnk_editar = self.browser.find_element_by_id('id_editar')
        lnk_editar.click()

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.clear()
        apellidos.send_keys('Arevalo Zapata')

        botonGrabar = self.browser.find_element_by_id('id_btn_editar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)

        link = self.browser.find_element_by_id('id_logout')
        link.click()
        self.browser.implicitly_wait(3)

        span=self.browser.find_element_by_xpath('//span[text()="Juan Daniel Arevalo Zapata"]')
        self.assertIn('Juan Daniel Arevalo Zapata', span.text)


    def test_registro_comentario(self):
        self.browser.get('http://localhost:8000')
        self.browser.implicitly_wait(3)
        span=self.browser.find_element_by_xpath('//span[text()="Juan Daniel Arevalo Zapata"]')
        span.click()
        self.browser.implicitly_wait(3)

        correo = self.browser.find_element_by_id('correo')
        correo.send_keys('xx@uniandes.edu.co')

        comentario = self.browser.find_element_by_id('comentario')
        comentario.send_keys('El comentario')

        btnComentar=self.browser.find_element_by_xpath('//button[@title="Adicionar comentario"]')
        btnComentar.click()

        paragraph=self.browser.find_element_by_xpath('//p[text()="El comentario"]')
        self.assertIn('El comentario', paragraph.text)